<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.specto.nl/SPECTO-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Lib
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Specto (http://www.magemonks.com)
 * @license     http://www.specto.nl/SPECTO-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Data_Form_Element_Select extends Varien_Data_Form_Element_Select{

    /**
     * This method was overriden to support 'disabled' options
     *
     * @param $option
     * @param $selected
     * @return string
     */
    protected function _optionToHtml($option, $selected)
    {
        if (is_array($option['value'])) {
            $html ='<optgroup label="'.$option['label'].'">'."\n";
            foreach ($option['value'] as $groupItem) {
                $html .= $this->_optionToHtml($groupItem, $selected);
            }
            $html .='</optgroup>'."\n";
        }
        else {
            $html = '<option value="'.$this->_escape($option['value']).'"';
            $html.= isset($option['title']) ? ' title="'.$this->_escape($option['title']).'"' : '';
            $html.= isset($option['style']) ? ' style="'.$option['style'].'"' : '';
            $html.= isset($option['disabled']) && $option['disabled'] == true ? '  disabled="disabled"' : '';
            if (in_array($option['value'], $selected)) {
                $html.= ' selected="selected"';
            }
            $html.= '>'.$this->_escape($option['label']). '</option>'."\n";
        }
        return $html;
    }

}