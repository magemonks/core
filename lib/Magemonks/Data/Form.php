<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.specto.nl/SPECTO-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Lib
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Specto (http://www.magemonks.com)
 * @license     http://www.specto.nl/SPECTO-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Data_Form extends Varien_Data_Form{
    public function setValues($values, $incremental = false)
    {
        foreach ($this->_allElements as $element) {
            if (isset($values[$element->getId()])) {
                $element->setValue($values[$element->getId()]);
            }
            elseif($incremental == false) {
                $element->setValue(null);
            }
        }
        return $this;
    }
}

