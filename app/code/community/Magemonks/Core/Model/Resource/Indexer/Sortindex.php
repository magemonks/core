<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Model_Resource_Indexer_Sortindex extends Mage_Catalog_Model_Resource_Product_Indexer_Abstract
{

    public function _construct()
    {
        $this->_init('magemonks/sortindex', 'entity_id');
    }

    /**
     * Reindex all
     */
    public function reindexAll()
    {
        $this->aggregate();
    }

    /**
     * Aggregate indexes by specific productIds
     *
     * @return Magemonks_Core_Model_Resource_Indexer_Sortindex
     * @throws Exception
     */
    public function aggregate()
    {
        $writeAdapter = $this->_getWriteAdapter();
        $this->beginTransaction();
        try {
            //empty the table
            $writeAdapter->delete($this->getTable('magemonks/sortindex'));


            //subquery for product views
            $views_select = $writeAdapter->select()
                ->from(array(
                        're' => $this->getTable('reports/event')
                    ),
                    array(
                        'entity_id' => 're.object_id',
                        'store_id',
                        'view_counter' => new Zend_Db_Expr('count(re.object_id)')
                    )
                )
                ->where('re.event_type_id = ?', Mage_Reports_Model_Event::EVENT_PRODUCT_VIEW)
                ->group(array('re.object_id', 're.store_id'));

            //subquery for sales entries
            $sale_select = $writeAdapter->select()
                ->from(array(
                        'soi' => $this->getTable('sales/order_item')
                    ),
                    array(
                        'entity_id' => 'soi.product_id',
                        'store_id',
                        'sale_counter' => new Zend_Db_Expr('sum(soi.qty_ordered)')
                    )
                )
                ->group(array('soi.product_id', 'soi.store_id'));

            //subquery for wishlist entries
            $wish_select = $writeAdapter->select()
                ->from(array(
                        'wi' => $this->getTable('wishlist/item')
                    ),
                    array(
                        'entity_id' => 'wi.product_id',
                        'store_id',
                        'wish_counter' => new Zend_Db_Expr('count(wi.product_id)')
                    )
                )
                ->group(array('wi.product_id', 'wi.store_id'));


            //subquery for rating entries
            $review_select = $writeAdapter->select()
                ->from(array(
                        'rra' => $this->getTable('review/review_aggregate')
                    ),
                    array(
                        'entity_id' => 'rra.entity_pk_value',
                        'store_id',
                        'reviews_count',
                        'rating_summary'
                    )
                )
                ->where('rra.entity_type = ?', 1)
                ->group(array('rra.entity_pk_value', 'rra.store_id'));


            //main query for selecting product data
            $select = $writeAdapter->select()
                ->from(
                    array(
                        'e' => $this->getTable('catalog/product'),
                    ),
                    array(
                        'e.entity_id',
                        'cs.store_id',
                        'view_counter'      => 'view.view_counter',
                        'sale_counter'      => 'sale.sale_counter',
                        'wish_counter'      => 'wish.wish_counter',
                        'rating_summary'    => 'review.rating_summary',
                        'review_counter'    => 'review.reviews_count'
                    )
                )
                ->join(
                    array('cs' => $this->getTable('core/store')),
                    '1',
                    array()
                )
                //Views
                ->joinLeft(
                    array('view' => $views_select),
                    'view.entity_id = e.entity_id AND view.store_id = cs.store_id',
                    array()
                )
                //Sale
                ->joinLeft(
                    array('sale' => $sale_select),
                    'sale.entity_id = e.entity_id AND sale.store_id = cs.store_id',
                    array()
                )
                //Wished
                ->joinLeft(
                    array('wish' => $wish_select),
                    'wish.entity_id = e.entity_id AND wish.store_id = cs.store_id',
                    array()
                )
                //Review
                ->joinLeft(
                    array('review' => $review_select),
                    'review.entity_id = e.entity_id AND review.store_id = cs.store_id',
                    array()
                )
                ->group(array('e.entity_id', 'cs.store_id'));

            $writeAdapter->query(
                $select->insertFromSelect($this->getTable('magemonks/sortindex'), array(
                    'entity_id',
                    'store_id',
                    'view_counter',
                    'sale_counter',
                    'wish_counter',
                    'rating_summary',
                    'review_counter',
                ))
            );

            $this->commit();
            Mage::dispatchEvent('magemonks_sorting_commit_after');
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }

        return $this;
    }
}