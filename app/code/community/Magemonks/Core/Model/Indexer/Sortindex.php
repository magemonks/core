<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Model_Indexer_Sortindex extends Mage_Index_Model_Indexer_Abstract {

    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('magemonks/indexer_sortindex', 'entity_id');
    }

    /**
     * Get Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('magemonks')->__('Magemonks Product Collection');
    }

    /**
     * Get Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return Mage::helper('magemonks')->__('Index product collection data used for sorting in widgets and menus.');
    }

    /**
     * Register indexer required data inside event object
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        //do nothing
    }

    /**
     * Process event based on event state data
     *
     * @param   Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        //do nothing
    }

    public function scheduledReindexAll()
    {
        $this->reindexAll();

        $process = Mage::getSingleton('index/indexer')->getProcessByCode('magemonks_sortindex');
        if (!is_null($process)) {
            $process->setStatus(Mage_Index_Model_Process::STATUS_PENDING);
            $process->setEndedAt(date('Y-m-d H:i:s'));
            $process->save();
        }
    }

}