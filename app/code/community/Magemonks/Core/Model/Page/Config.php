<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Model_Page_Config extends Mage_Page_Model_Config
{
    const XML_PATH_PAGE_REMOVE_LAYOUTS = 'global/page/remove_layouts';

    /**
     * Initialize page layouts list
     *
     * @return Labor_Templates_Model_Config
     */
    protected function _initPageLayouts()
    {
        parent::_initPageLayouts();
        return $this->_removePageLayouts(self::XML_PATH_PAGE_REMOVE_LAYOUTS);
    }

    /**
     * Remove page layouts
     *
     * @param $xmlPath
     * @return Magemonks_Core_Model_Page_Config
     */
    protected function _removePageLayouts($xmlPath)
    {
        if (!Mage::getConfig()->getNode($xmlPath) || !is_array($this->_pageLayouts)) {
            return $this;
        }
        foreach (explode(',', (string)Mage::getConfig()->getNode($xmlPath)->children()->layouts) as $toRemove) {
            unset($this->_pageLayouts[$toRemove]);
        }
        return $this;
    }
}