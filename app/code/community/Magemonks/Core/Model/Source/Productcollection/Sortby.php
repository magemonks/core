<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Model_Source_Productcollection_Sortby
{
    /**
     * Get the option array
     *
     * @param bool $addBlank
     * @param $extraOptions
     * @return array
     */
    public function toOptionArray($addBlank = true, $extraOptions = null)
    {
        $options = array(
            'name'              => Mage::helper('magemonks')->__('Product name'),
            'position'          => Mage::helper('magemonks')->__('Position'),
            'sku'               => Mage::helper('magemonks')->__('SKU'),
            'new_from_date'     => Mage::helper('magemonks')->__('New (from) date'),
            'created_at'        => Mage::helper('magemonks')->__('Created date'),
            'special_to_date'   => Mage::helper('magemonks')->__('Special price (to) date'),
            'special_from_date' => Mage::helper('magemonks')->__('Special price (from) date'),
            'highest_discount'  => Mage::helper('magemonks')->__('Highest Discount (%)'),
            'best_selling'      => Mage::helper('magemonks')->__('Best selling'),
            'best_rated'        => Mage::helper('magemonks')->__('Best Rated'),
            'most_viewed'       => Mage::helper('magemonks')->__('Most viewed'),
            'most_reviewed'     => Mage::helper('magemonks')->__('Most reviewed'),
            'most_sold'         => Mage::helper('magemonks')->__('Most sold'),
            'most_wished'       => Mage::helper('magemonks')->__('Most wished'),
            'highest_stock'     => Mage::helper('magemonks')->__('Highest stocklevel'),
        );

        if($addBlank){
            $options = array_merge(array('' => Mage::helper('magemonks')->__('--Please Select--')), $options);
        }

        if(is_array($extraOptions)){
            $options = array_merge($options, $extraOptions);
        }

        return $options;
    }
}