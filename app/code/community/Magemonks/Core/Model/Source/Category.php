<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Model_Source_Category
{
    const STORE_ROOT = 'STORE_ROOT_CATEGORY';

    public function toOptionArray($isMultiselect=false, $includeStoreRootCategory=false, $includeRootCategories = false)
    {

        $leaf = Mage::getBlockSingleton('adminhtml/urlrewrite_category_tree')
            ->getTreeArray(null, false, 999);

        $options = array();

        if(!$isMultiselect){
            $options[] = array(
                'value' => '',
                'label' => Mage::helper('magemonks')->__('--Please Select--'),
            );
        }
        if($includeStoreRootCategory){
            $options[] = array(
                'value' => self::STORE_ROOT,
                'label' => Mage::helper("magemonks")->__('-STORE ROOT CATEGORY-'),
            );
        }

        $options[] = array(
            'value' => $leaf['id'],
            'label' => str_repeat('-',$leaf['level']*2).$leaf['name'].' ('.$leaf['id'].')',
            'disabled' => !$includeRootCategories && $leaf['level'] <= 1 ? true : false
        );

        if(isset($leaf['children'])){
            $this->_processLeaves($leaf['children'], $options, true, $includeRootCategories);
        }

        return $options;

    }

    private function _processLeaves($leaves, &$options, $includeRootCategories){

        foreach($leaves as $leaf){
            $options[] = array(
                'value' => $leaf['id'],
                'label' => str_repeat('-',$leaf['level']*2).$leaf['name'].' ('.$leaf['id'].')',
                'disabled' => !$includeRootCategories && $leaf['level'] <= 1 ? true : false
            );

            if(isset($leaf['children'])){
                $this->_processLeaves($leaf['children'], $options, $includeRootCategories);
            }

        }
    }

    public function getCategoryIdFromValue($value){
        if($value == self::STORE_ROOT){
            return Mage::app()->getStore()->getRootCategoryId();
        }
        else{
            return $value;
        }
    }
}