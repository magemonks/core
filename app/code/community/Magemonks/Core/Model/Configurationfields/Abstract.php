<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
abstract class Magemonks_Core_Model_Configurationfields_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * Config fields saved (serialized) in de DB
     *
     * @var array
     */
    public $configFields = array();

    /**
     * Get the configuration fields
     *
     * @return array
     */
    public function getConfigurationFields(){
        return $this->configFields;
    }

    /**
     * Is the key a configuration field?
     *
     * @param $key
     * @return bool
     */
    public function isConfigurationField($key)
    {
        if(!is_string($key)) return false;
        $configurationFields = $this->getconfigurationFields();
        if(in_array($key, $configurationFields)){
            return true;
        }
        return false;
    }

    /**
     * Set a configuration value
     *
     * @param $key
     * @param null $value
     * @return Magemonks_Menumanager_Model_Item
     */
    public function setConfiguration($key, $value = null)
    {
        if(!$this->isConfigurationField($key)){
            return $this;
        }
        $configuration = unserialize($this->getData('configuration'));
        if(!is_array($configuration)){
            $configuration = array();
        }
        $configuration[$key] = $value;
        $this->setData('configuration', serialize($configuration));
        return $this;
    }

    /**
     * Get a configuration value
     *
     * @param null $key
     * @return array|mixed|null
     */
    public function getConfiguration($key = null)
    {
        $configuration = unserialize($this->getData('configuration'));
        if(!is_array($configuration)){
            $configuration = array();
        }
        if(is_null($key)){
            return $configuration;
        }
        if(isset($configuration[$key])){
            return $configuration[$key];
        }
        return null;
    }

    /**
     * Shorhand for the mix of getData / getConfiguration
     *
     * @param null $key
     * @param null $index
     * @return array|null
     */
    public function get($key = null, $index = null)
    {
        $data = $this->getData();
        $configuration = $this->getConfiguration();
        $merged = array_merge($data, $configuration);

        if(!empty($key)){
            if(array_key_exists($key, $merged)){
                $value = $merged[$key];
                if(!is_null($index) && is_array($value)){
                    if(is_string($index) && array_key_exists($index, $value)){
                        return $value[$index];
                    }
                    else{
                        return null;
                    }
                }
                return $value;
            }
            else{
                return null;
            }
        }

        return $merged;
    }


    /**
     * Shorthand for the mix of setData / setConfiguration
     *
     * @param $key
     * @param null $value
     * @return Magemonks_Menumanager_Model_Item
     */
    public function set($key, $value = null)
    {
        if(is_string($key)){
            if($this->isConfigurationField($key)){
                $this->setConfiguration($key, $value);
            }
            else{
                $this->setData($key, $value);
            }
        }
        elseif(is_array($key)){
            foreach($key as $_key => $value){
                if($this->isConfigurationField($_key)){
                    $this->setConfiguration($_key, $value);
                }
                else{
                    $this->setData($_key, $value);
                }
            }
        }
        return $this;
    }

}