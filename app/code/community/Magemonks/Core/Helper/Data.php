<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Helper_Data extends Mage_Core_Helper_Abstract{
    /**
     * Is the current URL the homepage
     * @return bool
     */
    public function isHomePage()
    {
        $helper = Mage::helper('core/url');
        return (bool) ($helper->getCurrentUrl() == $helper->getHomeUrl());
    }

    /**
     * Get a JSON wrapper
     *
     * @param $params
     * @return Magemonks_Core_Helper_Jsonwrapper
     */
    public function getJsonWrapper($params){
        if(!is_array($params)){
            Mage::ErrorException($this->__('The json wrapper only accepts arrays as parameter'));
        }
        return new Magemonks_Core_Helper_Jsonwrapper($params);
    }

    /**
     * Is Magento 1.7 or later?
     *
     * @return bool
     */
    public function isMageGte17()
    {
        return version_compare(Mage::getVersion(), '1.7', '>=');
    }
}