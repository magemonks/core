<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Helper_Coloredimage extends Mage_Core_Helper_Abstract{

    const MEDIA_DIR_COLORED_BACKGROUND = 'magemonks/coloredimage/coloredbackground/';
    const MEDIA_DIR_COLORED_IMAGE = 'magemonks/coloredimage/colorizedpng/';

    /**
     * Generate an image with solid color and returns the url
     *
     * @param $hex
     * @param int $width
     * @param int $height
     * @return string
     * @throws Exception
     */
    public function getSolidColorImageUrl($hex, $width = 100, $height = 100)
    {
        if(strpos($hex, '#') === 0){
            $hex = substr($hex, 1);
        }
        if(strlen($hex) != 6){
            throw new Exception("The HEX value should be exactly 6 characters.");
        }
        $isSecure = Mage::app()->getRequest()->isSecure();

        $destinationFileName = md5($hex.$width.$height).'.png';
        $destinationFileDirectory = Mage::getBaseDir('media') . DS . self::MEDIA_DIR_COLORED_BACKGROUND;

        try {
            $image = @imagecreate($width, $height);
            imagecolorallocate($image, hexdec($hex{0}.$hex{1}), hexdec($hex{2}.$hex{3}), hexdec($hex{4}.$hex{5}));
        } catch (Exception $e) {
            throw new Exception("Unable to create the image stream");
        }

        $this->_writeImage($image, $destinationFileDirectory, $destinationFileName);

        //return the url
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, $isSecure).self::MEDIA_DIR_COLORED_BACKGROUND.$destinationFileName;

    }

    /**
     * Takes a transparent image (png)
     *
     * @param $originalPath
     * @param $hex
     * @param bool $retina
     * @return string
     * @throws Exception
     */
    public function getColorizedPngUrl($originalPath, $hex, $retina = false)
    {
        if(strpos($hex, '#') === 0){
            $hex = substr($hex, 1);
        }
        if(strlen($hex) != 6){
            throw new Exception("The HEX value should be exactly 6 characters.");
        }

        $design = Mage::getDesign();
        $isSecure = Mage::app()->getRequest()->isSecure();

        $orignalFileLocation = $design->getFilename($originalPath, array('_type' => 'skin'));

        $destinationFileDirectory = Mage::getBaseDir('media') . DS . self::MEDIA_DIR_COLORED_IMAGE;
        $destinationFileName = md5($orignalFileLocation.$hex.(string)$retina).'.png';

        try {
            $image = imagecreatefrompng($orignalFileLocation);
        } catch (Exception $e) {
            throw new Exception("Unable to read the orignal PNG '{$orignalFileLocation}'.");
        }

        //create the colored image
        $rplus = -255 + hexdec($hex{0}.$hex{1});
        $gplus = -255 + hexdec($hex{2}.$hex{3});
        $bplus = -255 + hexdec($hex{4}.$hex{5});

        $imagex = imagesx($image);
        $imagey = imagesy($image);
        $coloredImage = imagecreatetruecolor($imagex, $imagey);
        imagesavealpha($coloredImage, true);
        imagealphablending($coloredImage, false);
        for ($x = 0; $x <$imagex; ++$x) {
            for ($y = 0; $y <$imagey; ++$y) {
                $rgb = imagecolorat($image, $x, $y);
                $color = imagecolorsforindex($image, $rgb);
                $grey = floor(($color['red']+$color['green']+$color['blue'])/3);

                $red = $grey + $rplus;
                $green = $grey + $gplus;
                $blue = $grey + $bplus;

                if ($red > 255) $red = 255;
                if ($green > 255) $green = 255;
                if ($blue > 255) $blue = 255;
                if ($red < 0) $red = 0;
                if ($green < 0) $green = 0;
                if ($blue < 0) $blue = 0;

                $newcol = imagecolorallocatealpha($coloredImage, $red,$green,$blue,$color['alpha']);
                imagesetpixel ($coloredImage, $x, $y, $newcol);
            }
        }

        $this->_writeImage($coloredImage, $destinationFileDirectory, $destinationFileName);

        //return the url
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, $isSecure).self::MEDIA_DIR_COLORED_IMAGE.$destinationFileName;
    }

    /**
     * @param $image resource
     * @param $destinationFileDirectory
     * @param $destinationFileName
     * @throws Exception
     */
    protected function _writeImage($image, $destinationFileDirectory, $destinationFileName)
    {
        if(empty($destinationFileDirectory)){
            throw new Exception("Destination directory can not be empty.");
        }

        $io = new Varien_Io_File();
        $destination = $destinationFileDirectory.DS.$destinationFileName;

        if(!$io->fileExists($destination) || true){
            if( !is_writable($destinationFileDirectory) ) {
                try {
                    $io = new Varien_Io_File();
                    $io->mkdir($destinationFileDirectory);
                } catch (Exception $e) {
                    throw new Exception("Unable to write file into directory '{$destinationFileDirectory}'. Access forbidden.");
                }
            }

            try {
                imagepng($image, $destination);
            } catch (Exception $e) {
                throw new Exception("Unable to save image into directory '{$destinationFileDirectory}'.");
            }
        }
    }
}