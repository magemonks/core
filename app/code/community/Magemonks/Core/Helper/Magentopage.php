<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Helper_Magentopage extends Mage_Core_Helper_Abstract{
    /**
     * Get the configuration for a particular page type
     *
     * @param null $type
     * @return array|null
     */
    public function getPage($type = null)
    {
        $configs = $this->_getConfig();

        if(is_string($type)){
            if(isset($configs[$type]) && is_array($configs[$type])){
                return $this->_processConfig($configs[$type]);
            }
            return null;
        }

        $page_config = array();
        foreach($configs as $key => $config){
            if(is_array($config)){
                $config = $this->_processConfig($config);
                if(is_array($config)){
                    $page_config[$key] = $config;
                }
            }
        }
        return $page_config;
    }

    /**
     * Process the config
     *
     * @param $config
     * @return array|null
     */
    protected function _processConfig($config)
    {
        if(isset($config['required_moudle']) && !Mage::helper('core')->isModuleOutputEnabled($config['required_moudle'])){
            return null;
        }
        if(isset($config['route'])){
            $config['url'] = Mage::getModel('core/url')->getUrl($config['route']);
        }
        if(isset($config['title'])){
            $title_set = false;
            try{
                if(isset($config['title_translation_helper'])){
                    $config['title'] = Mage::helper($config['title_translation_helper'])->__($config['title']);
                    $title_set = true;
                }
            }
            catch(Exception $e){ }
            try{
                if($title_set == false && $this->getData('default_title_translation_helper',null)){
                    $config['title'] = Mage::helper($this->getData('default_title_translation_helper'))->__($config['title']);
                }
            }
            catch(Exception $e){ }
        }
        return $config;
    }

    /**
     * Page definitions
     *
     * @return array
     */
    protected function _getConfig()
    {
        return array(
            'account' => array(
                'required_module'           => 'Mage_Customer',
                'title_translation_helper'  => 'Customer',
                'title'                     => 'My Account',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => true,
                'route'                     => 'customer/account',
            ),
            'wishlist' => array(
                'required_module'           => 'Mage_Wishlist',
                'title_translation_helper'  => 'Wishlist',
                'title'                     => 'My Wishlist',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => true,
                'route'                     => 'whishlist'
            ),
            'cart' => array(
                'required_module'           => 'Mage_Checkout',
                'title_translation_helper'  => 'Checkout',
                'title'                     => 'My Cart',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => true,
                'route'                     => 'checkout/cart'
            ),
            'checkout' => array(
                'required_module'           => 'Mage_Checkout',
                'title_translation_helper'  => 'Checkout',
                'title'                     => 'Checkout',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => true,
                'route'                     => 'checkout/cart'
            ),
            'login' => array(
                'required_module'           => 'Mage_Customer',
                'title_translation_helper'  => 'Customer',
                'title'                     => 'Login',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => false,
                'route'                     => 'customer/account/login'
            ),
            'logout' => array(
                'required_module'           => 'Mage_Customer',
                'title_translation_helper'  => 'Customer',
                'title'                     => 'Logout',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'customer/account/logout'
            ),
            'account_dashboard' => array(
                'required_module'           => 'Mage_Customer',
                'title_translation_helper'  => 'Customer',
                'title'                     => 'Account Dashboard',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'customer/account'
            ),
            'account_information' => array(
                'required_module'           => 'Mage_Customer',
                'title_translation_helper'  => 'Customer',
                'title'                     => 'Account Information',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'customer/account/edit'
            ),
            'address_book' => array(
                'required_module'           => 'Mage_Customer',
                'title_translation_helper'  => 'Customer',
                'title'                     => 'Address Book',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'customer/address'
            ),
            'orders' => array(
                'required_module'           => 'Mage_Sales',
                'title_translation_helper'  => 'Sales',
                'title'                     => 'My Orders',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'sales/order/history'
            ),
            'billing_agreements' => array(
                'required_module'           => 'Mage_Sales',
                'title_translation_helper'  => 'Core',
                'title'                     => 'Billing Agreements',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'sales/billing_agreement'
            ),
            'recurring_profiles' => array(
                'required_module'           => 'Mage_Sales',
                'title_translation_helper'  => 'Core',
                'title'                     => 'Recurring Profiles',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'sales/recurring_profile'
            ),
            'product_reviews' => array(
                'required_module'           => 'Mage_Review',
                'title_translation_helper'  => 'Review',
                'title'                     => 'My Product Reviews',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'review/customer'
            ),
            'tags' => array(
                'required_module'           => 'Mage_Tag',
                'title_translation_helper'  => 'Tag',
                'title'                     => 'My Tags',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'tag/customer'
            ),
            'applications' => array(
                'required_module'           => 'Mage_Oauth',
                'title_translation_helper'  => 'Oauth',
                'title'                     => 'My Applications',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'oauth/customer_token'
            ),
            'newsletter_subscriptions' => array(
                'required_module'           => 'Mage_Newsletter',
                'title_translation_helper'  => 'Newsletter',
                'title'                     => 'Newsletter Subscriptions',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'newsletter/manage'
            ),
            'downloadable_products' => array(
                'required_module'           => 'Mage_Downloadable',
                'title_translation_helper'  => 'Downloadable',
                'title'                     => 'My Downloadable Products',
                'display_when_logged_out'   => false,
                'display_when_logged_in'    => true,
                'route'                     => 'downloadable/customer/products'
            ),
            'sitemap_category' => array(
                'required_module'           => 'Mage_Catalog',
                'title_translation_helper'  => 'Catalog',
                'title'                     => 'Site Map (Category)',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => true,
                'route'                     => 'catalog/seo_sitemap/category'
            ),
            'sitemap_product' => array(
                'required_module'           => 'Mage_Catalog',
                'title_translation_helper'  => 'Catalog',
                'title'                     => 'Site Map (Product)',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => true,
                'route'                     => 'catalog/seo_sitemap/product'
            ),
            'search_terms' => array(
                'required_module'           => 'Mage_CatalogSearch',
                'title_translation_helper'  => 'CatalogSearch',
                'title'                     => 'Search Terms',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => true,
                'route'                     => 'catalogsearch/term/popular'
            ),
            'advanced_search' => array(
                'required_module'           => 'Mage_CatalogSearch',
                'title_translation_helper'  => 'CatalogSearch',
                'title'                     => 'Advanced Search',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => true,
                'route'                     => 'catalogsearch/advanced'
            ),
            'contact_us' => array(
                'required_module'           => 'Mage_Contacts',
                'title_translation_helper'  => 'Contacts',
                'title'                     => 'Contact Us',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => true,
                'route'                     => 'contacts'
            ),
            'orders_and_returns' => array(
                'required_module'           => 'Mage_Sales',
                'title_translation_helper'  => 'Sales',
                'title'                     => 'Orders and Returns',
                'display_when_logged_out'   => true,
                'display_when_logged_in'    => false,
                'route'                     => 'sales/guest/form'
            ),
        );
    }
}