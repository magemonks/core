<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Helper_License extends Mage_Core_Helper_Abstract{
    const XML_NODE_CRYPT_KEY = 'global/crypt/key';
    const SECRET = 'magemonks.com';

    /**
     * Generate the request code, based on product code
     *
     * @param $productCode
     * @return string
     */
    public function getRequestCode($productCode){
        $key = $this->getKey();
        $productCode = ucfirst(strtolower($productCode));

        if(empty($productCode) || empty($key)){
            return Mage::helper('magemonks')->__('An error occured');
        }

        $requestStirng =$key.'::'.$productCode;
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(self::SECRET), $requestStirng, MCRYPT_MODE_CBC, md5(md5(self::SECRET))));
    }

    /**
     * Is the serial valid for the given product code
     *
     * @param $serial
     * @param $productCode
     * @return bool
     */
    public function isSerialValid($serial, $productCode){
        $productCode = ucfirst(strtolower($productCode));
        $productCodeFromSerial = ucfirst(strtolower($this->getProductCodeFromSerial($serial)));
        return $productCode === $productCodeFromSerial;
    }

    /**
     * Get the productcode for the given serial
     *
     * @param $serial
     * @return string
     */
    public function getProductCodeFromSerial($serial){
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->getKey()), base64_decode($serial), MCRYPT_MODE_CBC, md5(md5($this->getKey()))), "\0");
    }

    /**
     * Get the key
     *
     * @return string
     */
    protected function getKey(){
        return substr((string) Mage::getConfig()->getNode(self::XML_NODE_CRYPT_KEY), 10);
    }
}