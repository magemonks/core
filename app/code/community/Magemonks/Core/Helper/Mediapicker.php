<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Helper_Mediapicker extends Mage_Core_Helper_Abstract{

    /**
     * Process the form value of an image
     *
     * @param $image
     * @return mixed|null
     */
    public function processFormValue($image)
    {
        if(!empty($image)){
            if(strpos($image,'/admin/cms_wysiwyg/directive/___directive/') !== false){
                $parts = explode('/',parse_url($image, PHP_URL_PATH));
                $key = array_search('___directive', $parts);
                if($key !== false){
                    $directive = $parts[$key+1];
                    $src = Mage::getModel('core/email_template_filter')->filter(Mage::helper('core')->urlDecode($directive));
                    if(!empty($src)){
                        return parse_url($src, PHP_URL_PATH);
                    }
                    return $image;
                }
                return $image;
            }
            return $image;
        }
        return $image;
    }

    public function getResizedUrl($originalPath, $width = null, $height = null, $constrainOnly = true, $keepAstpectRatio = false, $keepFrame = false)
    {
        if(empty($originalPath)){
            return null;
        }

        $originalPath = ltrim($originalPath, DS);

        if(is_null($width) && is_null($height)){
            return Mage::getBaseUrl().$originalPath;
        }

        $basePath = Mage::getBaseDir();

        $parts = explode(DS, $originalPath);
        $originalFile = array_pop($parts);
        $newPathRoot = array_shift($parts);
        $newPath = $newPathRoot.DS.'.resized'.DS.implode(DS,$parts);

        $parts = explode('.',$originalFile);
        $originalExtension = array_pop($parts);
        $newFileName = implode('.',$parts).'_'.(int)$width.'_'.(int)$height.'_'.(int)$constrainOnly.'_'.(int)$keepAstpectRatio.'_'.(int)$keepFrame.'.'.$originalExtension;


        $newUrl = Mage::getBaseUrl().$newPath.DS.$newFileName;
        $newPath = $basePath.DS.$newPath.DS.$newFileName;

        if (file_exists($originalPath) && is_file($originalPath) && !file_exists($newPath)) {
            $imageObj = new Varien_Image($originalPath);
            $imageObj->constrainOnly($constrainOnly);
            $imageObj->keepAspectRatio($keepAstpectRatio);
            $imageObj->keepFrame($keepFrame);
            $imageObj->resize($width, $height);
            $imageObj->save($newPath);
        }

        return $newUrl;
    }

}