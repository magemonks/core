<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Helper_Productcollection extends Mage_Core_Helper_Abstract {

    /**
     * Holds array with rules
     *
     * @var array
     */
    protected $_rules = array();

    /**
     * Holds array with filters
     *
     * @var array
     */
    protected $_filters = array();

    /**
     * Holds array with sortings
     *
     * @var array
     */
    protected $_sorting = array();

    /**
     * Holds array with attributes to select
     *
     * @var array
     */
    protected $_attributesToSelect = array();


    /**
     * Defines the amount of products in the collection
     *
     * @var null
     */
    protected $_limit = null;

    /**
     * Holds the base collection (if not defined, a new collection will be used)
     *
     * @var null|Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $_baseCollection = null;


    /**
     * Adds a rule
     *
     * @param Mage_CatalogRule_Model_Rule $rule
     * @return Magemonks_Core_Helper_Productcollection
     */
    public function addRule(Mage_CatalogRule_Model_Rule $rule)
    {
        $this->_rules[] = $rule;
        return $this;
    }

    /**
     * Adds a filter
     *
     * @param $field
     * @param $expression
     * @return Magemonks_Core_Helper_Productcollection
     */
    public function addFilter($field, $expression)
    {
        $this->_filters[] = array('field' => $field, 'expression' => $expression);
        return $this;
    }

    /**
     * Adds a sorting routine
     *
     * @param $field
     * @param string $direction
     * @param null|int $position
     * @return Magemonks_Core_Helper_Productcollection
     */
    public function addSorting($field, $direction = 'ASC', $position = null)
    {
        //@todo sort the orting by position
        $this->_sorting[] = array('field' => strtolower($field), 'direction' => strtoupper($direction), 'position' => $position);
        return $this;
    }

    /**
     * Sets the base collection
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @return Magemonks_Core_Helper_Productcollection
     */
    public function setBaseCollection(Mage_Catalog_Model_Resource_Product_Collection $collection)
    {
        $this->_baseCollection = $collection;
        return $this;
    }

    public function setLimit($count = null, $offset = null)
    {
        $this->_limit = array('count' => $count, 'offset' => $offset);
    }

    /**
     * Returns the collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getCollection()
    {
        //use base collection
        $collection = $this->_baseCollection;

        //no base collection was set
        if(is_null($collection)){
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->setStore(Mage::app()->getStore())
                ->addTaxPercents()
                ->addStoreFilter();

            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
        }

        if($collection->isLoaded()){
            Mage::throwException(Mage::helper('magemonks')->__('The collection was already loaded'));
        }

        //add rules
        foreach($this->_rules as $rule){
            $collection = $this->applyRuleToCollection($collection, $rule);
        }

        //add sorting
        foreach($this->_sorting as $sorting){
            $collection = $this->applySortingToCollection($collection, $sorting['field'], $sorting['direction']);
        }

        foreach($this->_filters as $filter){
            $collection = $this->applyFilterToCollection($collection, $filter['field'], $filter['expression']);
        }

        //add attribues to select
        if(count($this->_attributesToSelect)){
            foreach($this->_attributesToSelect as $attr){
                $collection->addAttributeToSelect($attr);
            }
        }
        else{
            $collection->addAttributeToSelect("*");
        }

        //add the limit
        if(is_array($this->_limit)){
            $collection->getSelect()->limit($this->_limit['count'], $this->_limit['offset']);
        }

        return $collection;
    }


    /**
     * Apply a rule to the collection
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param Mage_CatalogRule_Model_Rule $rule
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyRuleToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, Mage_CatalogRule_Model_Rule $rule)
    {
        $rule->setWebsiteIds(Mage::app()->getStore()->getWebsiteId());
        $collection->addAttributeToFilter("entity_id", array("in" => $rule->getMatchingProductIds()));
        return $collection;
    }


    /**
     * Apply a filter to the collection
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $field
     * @param $expression
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyFilterToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $field, $expression)
    {
        $filterMethod = $this->_fieldToFilterMethod($field);
        if($filterMethod){
            $collection = $this->$filterMethod($collection,  $expression);
        }
        else{
            $collection->addAttributeToFilter($field, $expression);
        }
        return $collection;
    }

    /**
     * Apply the in_stock_only filter
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param null $expression
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyHideOutOfStockFilterToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $expression = null)
    {
        $tableName = Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_status');
        if(false === $this->_isTableJoined($collection, $tableName)){
            Mage::getResourceModel('cataloginventory/stock_status')->addStockStatusToSelect($collection->getSelect(), Mage::app()->getWebsite());
        }

        $collection->getSelect()->where('stock_status.qty', array('gt' => 0));
        return $collection;
    }




    /**
     * Apply sorting to the collection
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $field
     * @param $direction
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applySortingToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $field, $direction)
    {
        $sortingMethod = $this->_fieldToSortingMethod($field);
        if($sortingMethod){
            $collection = $this->$sortingMethod($collection,  $direction);
        }
        else{
            $collection->addAttributeToSort($field, $direction);
        }
        return $collection;
    }


    /**
     * Apply has_stock sorting
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $direction
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyHasStockSortingToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $direction)
    {
        $tableName = Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_status');
        if(false === $this->_isTableJoined($collection, $tableName)){
            Mage::getResourceModel('cataloginventory/stock_status')->addStockStatusToSelect($collection->getSelect(), Mage::app()->getWebsite());
        }

        $collection->getSelect()->order(new Zend_Db_Expr('IF(stock_status.qty > 0, 1, 0) '.$direction));
        return $collection;
    }


    /**
     * Apply sorting by highest_stock
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $direction
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyHighestStockSortingToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $direction)
    {
        $tableName = Mage::getSingleton('core/resource')->getTableName('cataloginventory/stock_status');
        if(false === $this->_isTableJoined($collection, $tableName)){
            Mage::getResourceModel('cataloginventory/stock_status')->addStockStatusToSelect($collection->getSelect(), Mage::app()->getWebsite());
        }

        $collection->getSelect()->order(new Zend_Db_Expr('stock_status.qty '.$direction));
        return $collection;
    }

    /**
     * Apply sorting by highest_discount
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $direction
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyHighestDiscountSortingToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $direction)
    {
        $tableName = Mage::getSingleton('core/resource')->getTableName('catalog/product_index_price');
        if(false === $this->_isTableJoined($collection, $tableName)){
            $collection->addPriceData();
        }

        $collection->getSelect()->order(new Zend_Db_Expr('IF(price_index.price > 0 AND price_index.min_price > 0, ((price_index.min_price - price_index.price) / price_index.price * -100 ), 0) '.$direction));
        return $collection;
    }

    /**
     * Apply sorting by best_rated
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $direction
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyBestRatedSortingToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $direction)
    {
        return $this->_joinSortingIndexTable($collection)->addAttributeToSort('best_rated',  $direction);
    }

    /**
     * Apply sorting by most_sold
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $direction
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyMostSoldSortingToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $direction)
    {
        return $this->_joinSortingIndexTable($collection)->addAttributeToSort('most_sold',  $direction);
    }

    /**
     * Apply sorting by most_wished
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $direction
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyMostWishedSortingToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $direction)
    {
        return $this->_joinSortingIndexTable($collection)->addAttributeToSort('most_wished',  $direction);
    }

    /**
     * Apply sorting by most_viewed
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $direction
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyMostViewedSortingToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $direction)
    {
        return $this->_joinSortingIndexTable($collection)->addAttributeToSort('most_viewed',  $direction);
    }

    /**
     * Apply sorting by most_reviewd
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $direction
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function applyMostReviewedSortingToCollection(Mage_Catalog_Model_Resource_Product_Collection $collection, $direction)
    {
        return $this->_joinSortingIndexTable($collection)->addAttributeToSort('most_reviewed',  $direction);
    }




    /**
     * Get the sorting method
     *
     * @param $field
     * @return null|string
     */
    protected function _fieldToSortingMethod($field)
    {
        $field = uc_words($field);
        $field = str_replace('_', '', $field);
        $method = 'apply'.$field.'SortingToCollection';
        if(method_exists($this, $method)){
            return $method;
        }
        return null;
    }

    /**
     * Get the filter method
     *
     * @param $field
     * @return null|string
     */
    protected function _fieldToFilterMethod($field)
    {
        $field = uc_words($field);
        $field = str_replace('_', '', $field);
        $method = 'apply'.$field.'FilterToCollection';
        if(method_exists($this, $method)){
            return $method;
        }
        return null;
    }

    /**
     * Join the sorting index table, required for sorting on indexed attributes
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    protected function _joinSortingIndexTable(Mage_Catalog_Model_Resource_Product_Collection $collection)
    {
        $tableName = Mage::getSingleton('core/resource')->getTableName('magemonks/sortindex');
        if(false === $this->_isTableJoined($collection, $tableName)){
            $collection->joinTable(
                array('sortingindex' => 'magemonks/sortindex'), //table (alias / table)
                'entity_id = entity_id', //bind
                array( //fields (alias / field)
                    'most_viewed'   => 'view_counter',
                    'most_sold'     => 'sale_counter',
                    'most_wished'   => 'wish_counter',
                    'best_rated'    => 'rating_summary',
                    'most_reviewed' => 'review_counter'
                ),
                array ('store_id' => Mage::app()->getStore()->getId()), //condition
                'left' //jointype
            );
        }
        return $collection;
    }

    /**
     * Check if the table was joined
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $tableName
     * @return bool
     */
    protected function _isTableJoined(Mage_Catalog_Model_Resource_Product_Collection $collection, $tableName)
    {
        $joins = $collection->getSelect()->getPart(Zend_Db_Select::FROM);
        foreach($joins as $join){
            if($join['tableName'] === $tableName) return true;
        }
        return false;
    }

}