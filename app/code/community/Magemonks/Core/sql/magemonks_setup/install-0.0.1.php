<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */


/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'magemonks/productcollection_sortindex'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('magemonks/sortindex'))

    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Entity ID')

    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')

    ->addColumn('view_counter', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable'  => true,
        ), 'Viewed counter')

    ->addColumn('sale_counter', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable'  => true,
        ), 'Sold counter')

    ->addColumn('wish_counter', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable'  => true,
        ), 'Wished counter')

    ->addColumn('rating_summary', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable'  => true,
        ), 'Best rated')

    ->addColumn('review_counter', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable'  => true,
        ), 'review counter')

    ->addIndex($installer->getIdxName('magemonks/sortindex', array('store_id')),
        array('store_id'))

    ->addForeignKey($installer->getFkName('magemonks/sortindex', 'entity_id', 'catalog_product_entity', 'entity_id'),
        'entity_id', $installer->getTable('catalog_product_entity'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)

    ->addForeignKey($installer->getFkName('magemonks/sortindex', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

$installer->getConnection()->createTable($table);

$installer->endSetup();