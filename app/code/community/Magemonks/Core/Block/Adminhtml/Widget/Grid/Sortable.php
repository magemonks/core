<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Block_Adminhtml_Widget_Grid_Sortable extends Mage_Adminhtml_Block_Widget_Grid{

    protected $_defaultLimit    = 99999;
    protected $_defaultPage     = 1;
    protected $_defaultSort     = 'position';
    protected $_defaultDir      = 'asc';

    /**
     * Get the html (override some properties first)
     *
     * @return string
     */
    public function _toHtml()
    {
        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);

        $html = parent::_toHtml();
        $html .= '<p class="magemonks-sortable-dragdrop-notice">Drag &amp; Drop to sort / Click to edit</p>';
        $html .= $this->getSortableHtml();
        return $html;
    }

    /**
     * Prepend the javascript/css to the html
     *
     * @return string
     */
    public function getSortableHtml()
    {
        $html ="
            <script type='text/javascript'>
                //<![CDATA[
                (function($){
                    $(document).ready(function() {
                        //Fix the width of the columns
                        $('#".$this->getId()."_table colgroup col').each(function(index, element){
                            $(element).css('width', $(element).width());
                        });

                        $('#".$this->getId()."_table tbody tr').each(function(index, element){
                            var id = this.className.match(/sortable-id-(\d+)/)[1];
                            $(element).attr('id', 'sortable-id-'+id);
                        });

                        $('#".$this->getId()."_table tbody').sortable({
                            placeholder: 'ui-state-highlight',
                            forcePlaceholderSize: true,
                            helper: 'clone',
                            delay: 250,
                            start: function(event, ui){
                                ui.placeholder.html(ui.item.html());
                            },
                            stop: function(event, ui){
                                var itemData = $(this).sortable('toArray');
                                $.magemonksAdminhtmlAjax({
                                    'url': '".$this->getAjaxUrl()."',
                                    'data': {
                                        'itemData' : itemData,
                                    },
                                    'success' : function (response) {
                                        if($.type(response) === 'null'){
                                            $('#".$this->getId()."_table tbody').sortable('cancel');
                                        }
                                    },
                                    'error' : function(){
                                        $('#".$this->getId()."_table tbody').sortable('cancel');
                                    }
                                });                                

                                //$('#menumanagerMenuGrid_table tbody').sortable('cancel');
                                $(this).find('tr').removeClass('even odd first last');
                                decorateTable('".$this->getId()."_table');
                            }
                        });
                    });
                })(jQuery);
                //]]>
            </script>
            <style>
                #".$this->getId()."_table .ui-sortable-helper td { display: none; border-width: 0 }
                #".$this->getId()."_table .ui-sortable-helper td:first-child { display: table-cell;  }
                #".$this->getId()."_table .ui-state-highlight td {  }
            </style>
            ";
        return $html;
    }

    /**
     * Get the AJAX Url to post the sort data
     *
     * @return string
     */
    public function getAjaxUrl()
    {
        $url = $this->getUrl('*/*/sort', $this->getRequest()->getParams());
        return $url;
    }

    /**
     * Set an extra class to the row, used to get the object id
     *
     * @param $item
     * @return string
     */
    public function getRowClass($item)
    {
        $class = parent::getRowClass($item);
        return $class ? $class . ' sortable-id-' . $item->getId() : 'sortable-id-'.$item->getId();
    }

    /**
     * Sorting is disabled on the columns
     *
     * @param string $columnId
     * @param $column
     * @return Magemonks_Core_Block_Adminhtml_Widget_Grid_Sortable
     */
    public function addColumn($columnId, $column)
    {
        if(is_array($column)){
            $column['sortable'] = false;
        }
        parent::addColumn($columnId, $column);
        return $this;
    }

    /**
     * Simplified collection preperation
     *
     * @return this
     */
    protected function _prepareCollection()
    {
        if ($this->getCollection()) {

            $this->_preparePage();

            $columnId = $this->_defaultSort;
            $dir      = $this->_defaultDir;

            $collection = $this->getCollection();
            if ($collection) {
                $collection->setOrder($columnId, strtoupper($dir));
            }
        }
        return $this;
    }

    /**
     * Paging is disabled for the sortable grid
     *
     * @return Magemonks_Core_Block_Adminhtml_Widget_Grid_Sortable
     */
    protected function _preparePage()
    {
        return $this;
    }
}