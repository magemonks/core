<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Block_Adminhtml_Form_Element_Mediapicker extends Varien_Data_Form_Element_Text
{
    public function __construct($config)
    {
        parent::__construct($config);
        $this->addClass('magemonks-mediapicker-input');
        return $this;
    }

    /**
     * Retrieve additional html and put it at the end of element html
     *
     * @return string
     */
    public function getAfterElementHtml()
    {
        $html = "";
        $url = $this->getData('value');

        $html .= "<img src='".$url."' id='".$this->getHtmlId()."_preview' class='magemonks-mediapicker-preview".(empty($url) ? ' magemonks-mediapicker-preview-no-image' : '')."' />";

        $disabled = ($this->getDisabled() || $this->getReadonly());
        $html .= Mage::getSingleton('core/layout')
            ->createBlock('adminhtml/widget_button', '', array(
            'label'   => Mage::helper('magemonks')->__('Choose'),
            'type'    => 'button',
            'disabled' => $disabled,
            'class' => ($disabled) ? 'disabled magemonks-mediapicker-button' : 'magemonks-mediapicker-button',
            'onclick' => "MagemonksMediabrowserUtility.openDialog('" . Mage::helper('adminhtml')->getUrl('adminhtml/cms_wysiwyg_images/index', array('target_element_id' => $this->getHtmlId())) . "', '".$this->getHtmlId()."', '".Mage::helper('magemonks')->__("Media Picker")."')"
        ))->toHtml();

        $html .= Mage::getSingleton('core/layout')
            ->createBlock('adminhtml/widget_button', '', array(
            'label'   => Mage::helper('magemonks')->__('Remove'),
            'type'    => 'button',
            'disabled' => $disabled,
            'class' => ($disabled) ? 'disabled delete magemonks-mediapicker-button-remove' : 'delete magemonks-mediapicker-button-remove',
            'onclick' => "document.getElementById('".$this->getHtmlId()."').value = '';document.getElementById('".$this->getHtmlId()."_preview').src = ''; document.getElementById('".$this->getHtmlId()."_preview').className += 'magemonks-mediapicker-preview-no-image'; "
        ))->toHtml();

        $html .= parent::getAfterElementHtml();

        return $html;
    }
}

