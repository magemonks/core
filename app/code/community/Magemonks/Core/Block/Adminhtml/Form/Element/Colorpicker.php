<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker extends Varien_Data_Form_Element_Text
{
    public function __construct($config)
    {
        parent::__construct($config);
        $this->addClass("magemonks-colorpicker");
        $this->setData('tooltip', Mage::helper('magemonks')->__('Click the field to pick a color. The color values are in HEX.'));
        return $this;
    }

    /**
     * Get the after element HTML, used so a color picker is initialized when using ajax
     * @return string
     */
    public function getAfterElementHtml(){
        $html = "<script>new jscolor.color(document.getElementById('".$this->getHtmlId()."'), {hash:true,caps:false,required:false})</script>";
        return $html.parent::getAfterElementHtml();
    }
}