<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Block_Adminhtml_Form_Element_Storeselector extends Varien_Data_Form_Element_Select
{
    public function __construct($attributes = array())
    {
        parent::__construct($attributes);

        $values = array();
        $values[] = array('label' => '', 'value' => '');
        $websites = Mage::getModel('core/website')->getCollection();
        foreach($websites as $website){
            $values[] = array('label' => $website->getName(), 'value' => 'website_'.$website->getId(), 'style' => 'font-weight: bold;');

            $groups = $website->getGroups();
            foreach($groups as $group){
                $values[] = array('label' => $group->getName(), 'value' => 'group_'.$group->getId(), 'style' => 'font-weight: bold; text-indent: 15px;');

                $stores = $group->getStores();
                foreach($stores as $store){
                    $values[] = array('label' => $store->getName(), 'value' => 'store_'.$store->getId(), 'style' => 'text-indent: 25px;');
                }

            }
        }
        $this->setData('values', $values);
    }
}

