<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Block_Adminhtml_Form_Element_Renderer_System_Config_License_Serial
    extends Mage_Adminhtml_Block_System_Config_Form_Field implements Varien_Data_Form_Element_Renderer_Interface
{

    /**
     * Render the element
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $productCode = current(explode('_', $element->getId()));


        if($element->getEscapedValue() == ''){
            $element->setAfterElementHtml('<img style="position: absolute; margin: 2px 0 0 4px;" src="'.Mage::getDesign()->getSkinUrl('images/warning_msg_icon.gif').'" />');
        }
        elseif(Mage::helper('magemonks/license')->isSerialValid($element->getEscapedValue(), $productCode)){
            $element->setAfterElementHtml('<img style="position: absolute; margin: 2px 0 0 4px;" src="'.Mage::getDesign()->getSkinUrl('images/success_msg_icon.gif').'" />');
        }
        else{
            $element->setAfterElementHtml('<img style="position: absolute; margin: 2px 0 0 4px;" src="'.Mage::getDesign()->getSkinUrl('images/error_msg_icon.gif').'" />');
        }


        return parent::render($element);
    }
}