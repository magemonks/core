<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Block_Adminhtml_Form_Element_Gridselector_Grid_Product extends Mage_Adminhtml_Block_Catalog_Product_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setData('use_ajax', true);

        return $this;
    }

    /**
     * Prepare columns
     *
     * @return Magemonks_Core_Block_Adminhtml_Element_Gridselector_Grid_Product
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->removeColumn('action');
        $this->removeColumn('creation_time');
        $this->removeColumn('update_time');

        $this->sortColumnsByOrder();
        return $this;
    }

    /**
     * Row class
     *
     * @param $row
     * @return string
     */
    public function getRowClass($row)
    {
        if($row->getId() === $this->getData('item_id')){
            return 'magemonks-selected-grid-row';
        }
        return '';
    }

    /**
     * Click URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/*', array('_current'=>true));
    }

    protected function _prepareMassaction(){}

    /**
     * Row URL
     *
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return "javascript:magemonksGridSelector.selectElement({'id':'".$row->getId()."', 'description' : '".Mage::helper('core')->quoteEscape($row->getData('name'), true)."'})";
    }
}

