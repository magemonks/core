<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Block_Adminhtml_Form_Element_Gridselector_Cmspage extends Varien_Data_Form_Element_Text
{
    public function __construct($config)
    {
        parent::__construct($config);
        $this->addClass('magemonks-grid-selector');
        return $this;
    }


    /**
     * Retrieve additional html and put it at the end of element html
     *
     * @return string
     */
    public function getAfterElementHtml()
    {
        $html = "<span id='".$this->getHtmlId()."_description' class='magemonks-grid-selector-description'></span>";

        $disabled = ($this->getDisabled() || $this->getReadonly());
        $html .= Mage::getSingleton('core/layout')
            ->createBlock('adminhtml/widget_button', '', array(
            'label'   => Mage::helper('magemonks')->__('CMS Page Selector'),
            'type'    => 'button',
            'disabled' => $disabled,
            'class' => ($disabled) ? 'disabled magemonks-gridselector-button' : 'magemonks-gridselector-button',
            'onclick' => "magemonksGridSelector.open('".Mage::helper('adminhtml')->getUrl('adminhtml/magemonks_form/cmspage')."', '".$this->getHtmlId()."', $(".$this->getHtmlId().").value, '".Mage::helper('magemonks')->__('CMS Page Selector')."')"
        ))->toHtml();

        $html .= parent::getAfterElementHtml();

        return $html;
    }

}

