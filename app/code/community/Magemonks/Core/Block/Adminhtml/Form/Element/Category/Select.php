<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Block_Adminhtml_Form_Element_Category_Select extends Magemonks_Data_Form_Element_Select
{
    public function __construct($attributes)
    {
        parent::__construct($attributes);

        $includeStoreRootCategory = isset($attributes['includeStoreRootCategory']) && $attributes['includeStoreRootCategory'] === true ? true : false;
        $includeRootCategories = isset($attributes['includeRootCategories']) && $attributes['includeRootCategories'] === true ? true : false;

        $values = Mage::getModel('magemonks/source_category')->toOptionArray(false, $includeStoreRootCategory, $includeRootCategories);
        $this->setData('values', $values);

        return $this;
    }
}

