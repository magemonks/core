<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Block_Adminhtml_Form_Element_Gridselector_Grid_Cmspage extends Mage_Adminhtml_Block_Cms_Page_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setData('use_ajax', true);

        return $this;
    }

    /**
     * Prepare the column layout
     *
     * @return Magemonks_Core_Block_Adminhtml_Element_Gridselector_Grid_Cmspage
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->removeColumn('page_actions');
        $this->removeColumn('creation_time');
        $this->removeColumn('update_time');

        $this->sortColumnsByOrder();
        return $this;
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowClass($row)
    {
        if($row->getData('identifier') === $this->getData('item_id')){
            return 'magemonks-selected-grid-row';
        }
        return '';
    }

    /**
     * Get the grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/*', array('_current'=>true));
    }

    /**
     * Get the row click url
     *
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return "javascript:magemonksGridSelector.selectElement({'id':'".$row->getData('identifier')."', 'description' : ''})";
    }
}

