<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Core_Adminhtml_Magemonks_FormController extends Mage_Adminhtml_Controller_Action{
    /**
     * Product selector action for ajax request
     *
     */
    public function productAction()
    {
        $content = $this->getLayout()->createBlock('magemonks/adminhtml_form_element_gridselector_grid_product', '', array(
            'item_id' => $this->getRequest()->getParam('item_id', null),
        ));
        $this->getResponse()->setBody($content->toHtml());
    }

    /**
     * CMS Block selector action for ajax request
     *
     */
    public function cmsblockAction()
    {
        $content = $this->getLayout()->createBlock('magemonks/adminhtml_form_element_gridselector_grid_cmsblock', '', array(
            'item_id' => $this->getRequest()->getParam('item_id', null),
        ));
        $this->getResponse()->setBody($content->toHtml());
    }

    /**
     * CMS Page selector action for ajax request
     *
     */
    public function cmspageAction()
    {
        $content = $this->getLayout()->createBlock('magemonks/adminhtml_form_element_gridselector_grid_cmspage', '', array(
            'item_id' => $this->getRequest()->getParam('item_id', null),
        ));
        $this->getResponse()->setBody($content->toHtml());
    }

    /**
     * WYSIWYG editor action for ajax request
     *
     */
    public function wysiwygAction()
    {
        $elementId = $this->getRequest()->getParam('element_id', md5(microtime()));

        $content = $this->getLayout()->createBlock('magemonks/adminhtml_form_element_wysiwyg_content', '', array(
            'editor_element_id' => $elementId,
        ));

        $this->getResponse()->setBody($content->toHtml());
    }

    /**
     *  Is it allowed to access this controller?
     *
     * @return Boolean isAllowed
     */
    protected function _isAllowed()
    {
        return true;
    }

}