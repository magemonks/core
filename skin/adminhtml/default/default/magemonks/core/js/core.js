/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.nl/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.nl>
 * @copyright   2012 Magemonks (http://www.magemonks.nl)
 * @license     http://www.magemonks.nl/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */

//Functions in the global namespace (due to the mix of prototype and jquery).
var magemonksProcessjsonResponse,
    magemonksHideLoadingMask,
    magemonksShowLoadingMask,
    magemonksBeforeAjaxRequest,
    magemonksAfterAjaxRequest;


(function($){

    magemonksHideLoadingMask = function(){
        $('#loading-mask').hide();
    }
    magemonksShowLoadingMask = function(){
        var loadingMask = $('#loading-mask');
        loadingMask.height($('body').height());
        loadingMask.width($('body').width());
        loadingMask.css('top', $('body').scrollTop());
        loadingMask.show();
    }
    magemonksBeforeAjaxRequest = function(showLoadingMask){
        $('#messages').html('');
        if(showLoadingMask){
            magemonksShowLoadingMask();
        }
    }

    magemonksAfterAjaxRequest = function(){
        magemonksHideLoadingMask();
    }
    magemonksProcessjsonResponse = function(response){
        if(response.ajaxExpired && response.ajaxRedirect) {
            document.location.href = response.ajaxRedirect;
            return null;
        }
        if(response.error){
            if(response.message){
                alert(response.message);
            }
            else{
                alert('An unkown error occured');
            }
            return null;
        }
        if(response.messages){
            $('#messages').html(response.messages);
        }
        return response;
    }



    $.magemonksAdminhtmlAjax = function(options) {
        var defaults = {
            "type" : "POST",
            "dataType": "json"
        };
        options = $.extend(defaults, options);
        options.data = $.extend({
                "form_key": FORM_KEY,
                "isAjax" : "true"
            }, options.data );


        magemonksBeforeAjaxRequest(options.disableLoadingMask ? false : true);

        var orignalSucces = options.success;
        options.success = function(response){
            response = magemonksProcessjsonResponse(response);

            if($.type(orignalSucces) === 'function'){
                return orignalSucces(response);
            }
            return response;
        }

        return $.ajax(options).always(function(){
            magemonksAfterAjaxRequest();
        });
    };
})(jQuery);


var MagemonksMediabrowserUtility = {
    elementId : null,
    openDialog: function(url, elementId, title) {
        this.elementId = elementId;
        if ($('browser_window') && typeof(Windows) != 'undefined') {
            Windows.focus('browser_window');
            return;
        }
        this.dialogWindow = Dialog.info(null, {
            closable:     true,
            resizable:    false,
            draggable:    true,
            className:    'magento',
            windowClassName:    'popup-window',
            title:        title || 'Insert File...',
            top:          50,
            width:        950,
            height:       600,
            zIndex:       1000,
            recenterAuto: false,
            hideEffect:   Element.hide,
            showEffect:   Element.show,
            id:           'browser_window',
            onClose: this.closeDialog.bind(this)
        });
        new Ajax.Updater('modal_dialog_message', url, {evalScripts: true});
    },
    closeDialog: function(window) {
        if (!window) {
            window = this.dialogWindow;
        }
        if (window) {
            // IE fix - hidden form select fields after closing dialog
            WindowUtilities._showSelect();
            window.close();
        }
        //because change events on elementId are not fired (?) this nasty construction.
        var elementId = this.elementId;
        var timeOut = setTimeout(function(){
            if($(elementId).value && $(elementId).value != ''){
                $(elementId+'_preview').setAttribute('src', $(elementId).value);
                $(elementId+'_preview').removeClassName('magemonks-mediapicker-preview-no-image');
            }
        },50);
    }
};

var magemonksGridSelector = {
    overlayShowEffectOptions : null,
    overlayHideEffectOptions : null,
    elementId : null,
    dialogWindow : null,
    open : function(editorUrl, elementId, itemId, dialogTitle) {
        this.elementId = elementId;
        if (editorUrl) {
            new Ajax.Request(editorUrl, {
                parameters: {
                    item_id: itemId
                },
                onSuccess: function(transport) {
                    try {
                        this.openDialogWindow(transport.responseText, dialogTitle);
                    } catch(e) {
                        alert(e.message);
                    }
                }.bind(this)
            });
        }
    },
    openDialogWindow : function(content, dialogTitle) {
        this.overlayShowEffectOptions = Windows.overlayShowEffectOptions;
        this.overlayHideEffectOptions = Windows.overlayHideEffectOptions;
        Windows.overlayShowEffectOptions = {duration:0};
        Windows.overlayHideEffectOptions = {duration:0};

        this.dialogWindow = Dialog.info(content, {
            draggable:true,
            resizable:true,
            closable:true,
            className:"magento",
            windowClassName:"popup-window",
            title: dialogTitle,
            width:1020,
            height: 500,
            zIndex:1000,
            recenterAuto:false,
            hideEffect:Element.hide,
            showEffect:Element.show,
            id:"magemonks-grid-selector",
            onClose: this.closeDialogWindow.bind(this)
        });

        content.evalScripts.bind(content).defer();

    },
    closeDialogWindow : function(dialogWindow) {
        $(this.elementId).focus();
        dialogWindow.close();
        Windows.overlayShowEffectOptions = this.overlayShowEffectOptions;
        Windows.overlayHideEffectOptions = this.overlayHideEffectOptions;
    },
    selectElement: function(response){
        $(this.elementId).value = response.id;
        $(this.elementId+'_description').update(response.description);
        this.dialogWindow.close();
    }
};


var magemonksWysiwygEditor = {
    overlayShowEffectOptions : null,
    overlayHideEffectOptions : null,
    open : function(editorUrl, elementId) {
        if (editorUrl && elementId) {
            new Ajax.Request(editorUrl, {
                parameters: {
                    element_id: elementId+'_editor',
                    store_id: '<?php echo $this->getStoreId() ?>'
                },
                onSuccess: function(transport) {
                    try {
                        this.openDialogWindow(transport.responseText, elementId);
                    } catch(e) {
                        alert(e.message);
                    }
                }.bind(this)
            });
        }
    },
    openDialogWindow : function(content, elementId) {
        this.overlayShowEffectOptions = Windows.overlayShowEffectOptions;
        this.overlayHideEffectOptions = Windows.overlayHideEffectOptions;
        Windows.overlayShowEffectOptions = {duration:0};
        Windows.overlayHideEffectOptions = {duration:0};

        Dialog.confirm(content, {
            draggable:true,
            resizable:true,
            closable:true,
            className:"magento",
            windowClassName:"popup-window",
            title:'WYSIWYG Editor',
            width:950,
            height:555,
            zIndex:1000,
            recenterAuto:false,
            hideEffect:Element.hide,
            showEffect:Element.show,
            id:"catalog-wysiwyg-editor",
            buttonClass:"form-button",
            okLabel:"Submit",
            ok: this.okDialogWindow.bind(this),
            cancel: this.closeDialogWindow.bind(this),
            onClose: this.closeDialogWindow.bind(this),
            firedElementId: elementId
        });

        content.evalScripts.bind(content).defer();

        $(elementId+'_editor').value = $(elementId).value;
    },
    okDialogWindow : function(dialogWindow) {
        if (dialogWindow.options.firedElementId) {
            wysiwygObj = eval('wysiwyg'+dialogWindow.options.firedElementId+'_editor');
            wysiwygObj.turnOff();
            if (tinyMCE.get(wysiwygObj.id)) {
                $(dialogWindow.options.firedElementId).value = tinyMCE.get(wysiwygObj.id).getContent();
            } else {
                if ($(dialogWindow.options.firedElementId+'_editor')) {
                    $(dialogWindow.options.firedElementId).value = $(dialogWindow.options.firedElementId+'_editor').value;
                }
            }
        }
        this.closeDialogWindow(dialogWindow);
    },
    closeDialogWindow : function(dialogWindow) {
        // remove form validation event after closing editor to prevent errors during save main form
        if (typeof varienGlobalEvents != undefined && editorFormValidationHandler) {
            varienGlobalEvents.removeEventHandler('formSubmit', editorFormValidationHandler);
        }

        //IE fix - blocked form fields after closing
        $(dialogWindow.options.firedElementId).focus();

        //destroy the instance of editor
        wysiwygObj = eval('wysiwyg'+dialogWindow.options.firedElementId+'_editor');
        if (tinyMCE.get(wysiwygObj.id)) {
            tinyMCE.execCommand('mceRemoveControl', true, wysiwygObj.id);
        }

        dialogWindow.close();
        Windows.overlayShowEffectOptions = this.overlayShowEffectOptions;
        Windows.overlayHideEffectOptions = this.overlayHideEffectOptions;
    }
};